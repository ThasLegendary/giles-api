use std::env;

extern crate database;
use database::db;
use database::db::model::*;

use dotenv::dotenv;

use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;

mod filesystem;
mod metadata;

fn main() -> io::Result<()> {
    dotenv().ok();

    let cover_path = env::var("GILES_PICTURES_PATH").expect("GILES_PICTURES_PATH must be set");
    println!("create cover folder: {}", &cover_path);
    fs::create_dir_all(&cover_path)?;

    let library_path = env::var("GILES_LIBRARY_PATH").expect("GILES_LIBRARY_PATH must be set");
    println!("scan target: {}", library_path);

    let conn_pool = db::establish_connection();

    // TODO error handling fir all unwraps
    let audiobooks = filesystem::get_audiobooks(&library_path).unwrap();

    let conn = conn_pool.get().unwrap();
    for audiobook in audiobooks.iter() {
        let audiobook_insert = Audiobook::create(
            &audiobook.path,
            audiobook.title.as_deref(),
            audiobook.author.as_deref(),
            audiobook.reader.as_deref(),
            audiobook.genre.as_deref(),
            audiobook.year,
            audiobook.picture_extension.as_deref(),
            &conn,
        );

        if audiobook_insert.is_some() {
            let audiobook_record = audiobook_insert.unwrap();

            if let Some(cover) = &audiobook_record.cover {
                if let Some(data) = &audiobook.picture_content {
                    let mut file = File::create(format!("{}/{}", &cover_path, cover))?;
                    let _res = file.write_all(&data); //TODO
                }
            }

            let chapters = filesystem::get_chapters(&audiobook.path).unwrap();
            for chapter in chapters.iter() {
                Chapter::create(
                    &audiobook_record.id,
                    &chapter.path,
                    chapter.title.as_deref(),
                    chapter.track,
                    &conn,
                );
            }
        }
    }
    Ok(())
}
