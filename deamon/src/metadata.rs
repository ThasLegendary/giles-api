pub struct AudiobookData {
    pub path: String,
    pub title: Option<String>,
    pub author: Option<String>,
    pub reader: Option<String>,
    pub genre: Option<String>,
    pub year: Option<i32>,
    pub picture_extension: Option<String>,
    pub picture_content: Option<Vec<u8>>,
}

impl AudiobookData {
    pub fn new(
        path: &str,
        title: Option<&str>,
        author: Option<&str>,
        reader: Option<&str>,
        genre: Option<&str>,
        year: Option<i32>,
        picture_extension: Option<&str>,
        picture_content: Option<Vec<u8>>,
    ) -> Self {
        Self {
            path: path.into(),
            title: title.map(Into::into),
            author: author.map(Into::into),
            reader: reader.map(Into::into),
            genre: genre.map(Into::into),
            year,
            picture_extension: picture_extension.map(Into::into),
            picture_content,
        }
    }
}

pub struct ChapterData {
    pub path: String,
    pub title: Option<String>,
    pub track: Option<i32>,
}

impl ChapterData {
    pub fn new(path: &str, title: Option<&str>, track: Option<i32>) -> Self {
        Self {
            path: path.into(),
            title: title.map(Into::into),
            track,
        }
    }
}
