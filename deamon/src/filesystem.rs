extern crate database;

use crate::metadata::AudiobookData;
use crate::metadata::ChapterData;

use std::io;
use walkdir::WalkDir;

pub fn get_audiobooks(library_path: &str) -> Result<Vec<AudiobookData>, io::Error> {
    let mut library = Vec::new();
    let mut dirs = Vec::new();

    for entry in WalkDir::new(library_path)
        .follow_links(true)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();
        let d_name = String::from(entry.path().parent().unwrap().to_string_lossy());

        if f_name.ends_with(".mp3") && !dirs.contains(&d_name) {
            dirs.push(d_name.clone());
            match id3::Tag::read_from_path(entry.path()) {
                Ok(tag) => {
                    let mut pic_ext: Option<&str> = None;
                    let mut pic_data: Option<Vec<u8>> = None;
                    for picture in tag.pictures() {
                        if picture.picture_type == id3::frame::PictureType::CoverFront {
                            pic_ext = Some(mime_to_ext(&picture.mime_type));
                            pic_data = Some(picture.data.clone());
                            break;
                        }
                    }
                    let audiobook_data = AudiobookData::new(
                        &d_name,
                        tag.album(),
                        tag.artist(),
                        tag.album_artist(),
                        tag.genre(),
                        tag.year(),
                        pic_ext,
                        pic_data,
                    );

                    library.push(audiobook_data);
                }
                Err(e) => println!("Error {} on {:?}", e, entry.path()),
            };
        }
    }

    Ok(library)
}

pub fn get_chapters(audiobook_path: &str) -> Result<Vec<ChapterData>, io::Error> {
    let mut chapters = Vec::new();

    for entry in WalkDir::new(audiobook_path)
        .follow_links(true)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.path().to_string_lossy();

        if f_name.ends_with(".mp3") {
            match id3::Tag::read_from_path(entry.path()) {
                Ok(tag) => {
                    let track_nb = match tag.track() {
                        Some(nb) => Some(nb as i32),
                        None => None,
                    };
                    let chapter = ChapterData::new(&f_name, tag.title(), track_nb);
                    chapters.push(chapter);
                }
                Err(e) => println!("Error {} on {:?}", e, entry.path()),
            };
        }
    }

    Ok(chapters)
}

fn mime_to_ext(mime: &str) -> &str {
    let split = mime.split("/");
    let parts = split.collect::<Vec<&str>>();
    parts[1]
}
