#[macro_use]
extern crate actix_web;
extern crate database;

use std::{env, io};

use actix_files;
use actix_web::{middleware, web, App, HttpServer};

mod config;
mod constants;
mod library;
mod response;

use config::Config;
use database::db;

use dotenv::dotenv;

use std::fs;

#[actix_web::main]
async fn main() -> io::Result<()> {
    dotenv().ok();

    let cover_path = env::var("GILES_PICTURES_PATH").expect("GILES_PICTURES_PATH must be set");
    println!("create cover folder: {}", &cover_path);
    fs::create_dir_all(&cover_path)?;

    let cover_prefix = env::var("GILES_COVER_PREFIX").expect("GILES_COVER_PREFIX must be set");

    let config = Config::new(cover_prefix);
    let conn_pool = db::establish_connection();

    HttpServer::new(move || {
        App::new().wrap(middleware::Logger::default()).service(
            web::scope("/giles")
                .data(config.clone())
                .data(conn_pool.clone())
                .service(
                    actix_files::Files::new(&config.cover_prefix, &cover_path).show_files_listing(),
                )
                .service(library::list),
        )
    })
    .bind("127.0.0.1:9090")?
    .run()
    .await
}
