use actix_web::{web, HttpResponse};

use crate::config::Config;
use crate::constants::APPLICATION_JSON;
use crate::response::Response;

extern crate database;

use database::db::model::Audiobook;
use database::db::DbPool;

pub type Audiobooks = Response<Audiobook>;

#[get("/audiobooks")]
pub async fn list(data: web::Data<Config>, pool: web::Data<DbPool>) -> HttpResponse {
    let conn = pool.get().unwrap();
    let mut library: Vec<Audiobook> = Audiobook::list(&conn);

    for mut audiobook in library.iter_mut() {
        if let Some(cover) = &audiobook.cover {
            audiobook.cover = Some(data.cover_prefix.to_owned() + "/" + cover);
        }
    }

    let audiobooks = Audiobooks {
        results: library, //TODO err handling
    };

    HttpResponse::Ok()
        .content_type(APPLICATION_JSON)
        .json(audiobooks)
}
