#[derive(Debug, Clone)]
pub struct Config {
    pub cover_prefix: String,
}

impl Config {
    pub fn new(cover_prefix: String) -> Self {
        Self { cover_prefix }
    }
}
