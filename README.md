# Giles - Audiobook library to API
[![forthebadge](https://forthebadge.com/images/badges/made-with-rust.svg)](https://forthebadge.com)

Set of tools to create an API from your audiobook folder.

# Table of contents

- [Install](#install)
  - [Linux](#linux)
- [Config](#config)
- [License](#license)

# Installation

[(Back to top)](#table-of-contents)

### Linux
```sh
sudo apt install build-essential
sudo apt install sqlite3 libsqlite3-0 libsqlite3-dev
cargo install diesel_cli --no-default-features --features sqlite

cargo build
```

# Config
[(Back to top)](#table-of-contents)

In order to work binaries expect following environment variables, they can be defined using a .env file.

```sh
DATABASE_URL=database/giles.db
GILES_PICTURES_PATH=/mnt/e/Workspace/giles-api/pictures
GILES_LIBRARY_PATH=/mnt/e/Audiobooks/
GILES_COVER_PREFIX=/img
```

DATABASE_URL is the path to the sqlite database that is storing audiobook data (api + deamon)
GILES_PICTURES_PATH is the path where audiobook covers will be extracted to (deamon)
GILES_LIBRARY_PATH is the path to your audiobook library (deamon)
GILES_COVER_PREFIX is the prefix used for covers urls (api)

# License
[(Back to top)](#table-of-contents)