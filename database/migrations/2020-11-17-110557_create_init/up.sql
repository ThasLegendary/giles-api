-- audiobooks
CREATE TABLE IF NOT EXISTS audiobook (
  id TEXT PRIMARY KEY NOT NULL,
  path TEXT NOT NULL UNIQUE,
  title TEXT,
  author TEXT,
  reader TEXT,
  genre TEXT,
  year INTEGER,
  cover TEXT,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE TRIGGER IF NOT EXISTS UpdateTimestamps
AFTER
UPDATE ON audiobook FOR EACH ROW
  WHEN NEW.updated_at <= OLD.updated_at BEGIN
update audiobook
set updated_at = CURRENT_TIMESTAMP
where id = OLD.id;
END;
CREATE TABLE IF NOT EXISTS chapter (
  id TEXT PRIMARY KEY NOT NULL,
  audiobook_id TEXT NOT NULL,
  path TEXT NOT NULL UNIQUE,
  title TEXT,
  track INTEGER,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE TRIGGER IF NOT EXISTS UpdateTimestamps
AFTER
UPDATE ON chapter FOR EACH ROW
  WHEN NEW.updated_at <= OLD.updated_at BEGIN
update chapter
set updated_at = CURRENT_TIMESTAMP
where id = OLD.id;
END;