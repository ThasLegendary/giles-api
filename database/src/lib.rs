#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
pub mod db;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
