use crate::db::{establish_connection, model::Audiobook};
#[test]
fn create_audiobook() {
    let conn = establish_connection().get().unwrap();

    let path = "/share/audiobooks/harry potter 1/";
    let title = Some("Harry Potter à l'école des sorciers");
    let author = Some("J.K. Rowling");
    let reader = Some("Jean-Piere Papin");
    let genre = Some("Jeune lecteur");
    let year = Some(1998);

    let audiobook = Audiobook::create_raw(path, title, author, reader, genre, year, &conn).unwrap();
    assert_eq!(audiobook.path.as_str(), path);
    assert_eq!(audiobook.title.unwrap().as_str(), title.unwrap());
    assert_eq!(audiobook.author.unwrap().as_str(), author.unwrap());
    assert_eq!(audiobook.reader.unwrap().as_str(), reader.unwrap());
    assert_eq!(audiobook.genre.unwrap().as_str(), genre.unwrap());
    assert_eq!(audiobook.year.unwrap(), year.unwrap());
}

#[test]
fn list_users() {
    let conn = establish_connection().get().unwrap();
    let path = "/share/audiobooks/harry potter 2/";
    let title = Some("Harry Potter et la chambre des secrets");
    let author = Some("J.K. Rowling");
    let reader = Some("Ginola");
    let genre = Some("Jeune lecteur");
    let year = Some(1999);

    let audiobook = Audiobook::create_raw(path, title, author, reader, genre, year, &conn).unwrap();

    let existing_audiobooks = Audiobook::list(&conn);
    assert_eq!(1, existing_audiobooks.len());
    assert_eq!(audiobook.id, existing_audiobooks[0].id);
}

#[test]
fn get_user_by_id() {
    let conn = establish_connection().get().unwrap();
    let path = "/share/audiobooks/harry potter 3/";
    let title = Some("Harry Potter et le prisonnier d'Azkaban");
    let author = Some("J.K. Rowling");
    let reader = Some("Zinedine Zidane");
    let genre = Some("Jeune lecteur");
    let year = Some(2000);

    let audiobook = Audiobook::create_raw(path, title, author, reader, genre, year, &conn).unwrap();
    let existing_audiobooks = Audiobook::by_id(&audiobook.id, &conn).unwrap();
    assert_eq!(audiobook.id, existing_audiobooks.id);
}
