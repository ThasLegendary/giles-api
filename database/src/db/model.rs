use super::schema::audiobook;
use super::schema::audiobook::dsl::audiobook as audiobook_dsl;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Deserialize, Serialize, Queryable, Insertable, AsChangeset)]
#[changeset_options(treat_none_as_null = "true")]
#[table_name = "audiobook"]
pub struct Audiobook {
    pub id: String,
    pub path: String,
    pub title: Option<String>,
    pub author: Option<String>,
    pub reader: Option<String>,
    pub genre: Option<String>,
    pub year: Option<i32>,
    pub cover: Option<String>,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

impl Audiobook {
    pub fn list(conn: &SqliteConnection) -> Vec<Self> {
        audiobook_dsl
            .load::<Audiobook>(conn)
            .expect("Error loading users")
    }
    pub fn by_id(id: &str, conn: &SqliteConnection) -> Option<Self> {
        if let Ok(record) = audiobook_dsl.find(id).get_result::<Audiobook>(conn) {
            Some(record)
        } else {
            None
        }
    }
    fn by_path(path_str: &str, conn: &SqliteConnection) -> Option<Self> {
        use super::schema::audiobook::dsl::path;
        if let Ok(record) = audiobook_dsl
            .filter(path.eq(path_str))
            .first::<Audiobook>(conn)
        {
            Some(record)
        } else {
            None
        }
    }
    fn to_cover_file(id: &str, cover: Option<&str>) -> Option<String> {
        let mut filename: Option<String> = None;
        let mut joined: String = String::new();
        if let Some(ext) = cover {
            joined = id.to_owned() + "." + ext;
            filename = Some(joined);
        }
        filename
    }

    pub fn create(
        path: &str,
        title: Option<&str>,
        author: Option<&str>,
        reader: Option<&str>,
        genre: Option<&str>,
        year: Option<i32>,
        cover: Option<&str>,
        conn: &SqliteConnection,
    ) -> Option<Self> {
        // possible data validation here

        if let Some(existing_record) = Self::by_path(path, conn) {
            use super::schema::audiobook::dsl::id;
            let filename = Self::to_cover_file(&existing_record.id, cover);
            let new_audiobook = Self::new_audiobook_struct(
                &existing_record.id,
                path,
                title,
                author,
                reader,
                genre,
                year,
                filename,
            );
            diesel::update(audiobook_dsl.filter(id.eq(&existing_record.id)))
                .set(&new_audiobook)
                .execute(conn)
                .expect("Error updating audiobook");
            Self::by_id(&existing_record.id, conn)
        } else {
            let new_id = Uuid::new_v4().to_hyphenated().to_string();
            let filename = Self::to_cover_file(&new_id, cover);
            let new_audiobook = Self::new_audiobook_struct(
                &new_id, path, title, author, reader, genre, year, filename,
            );
            diesel::insert_into(audiobook_dsl)
                .values(&new_audiobook)
                .execute(conn)
                .expect("Error saving new audiobook");
            Self::by_id(&new_id, conn)
        }
    }

    pub fn new_audiobook_struct(
        id: &str,
        path: &str,
        title: Option<&str>,
        author: Option<&str>,
        reader: Option<&str>,
        genre: Option<&str>,
        year: Option<i32>,
        cover: Option<String>,
    ) -> Self {
        Self {
            id: id.into(),
            path: path.into(),
            title: title.map(Into::into),
            author: author.map(Into::into),
            reader: reader.map(Into::into),
            genre: genre.map(Into::into),
            year,
            cover: cover,
            created_at: chrono::Local::now().naive_local(),
            updated_at: chrono::Local::now().naive_local(),
        }
    }
}

#[cfg(test)]
mod audiobook_test;

use super::schema::chapter;
use super::schema::chapter::dsl::chapter as chapter_dsl;

#[derive(Debug, Deserialize, Serialize, Queryable, Insertable)]
#[table_name = "chapter"]
pub struct Chapter {
    pub id: String,
    pub audiobook_id: String,
    pub path: String,
    pub title: Option<String>,
    pub track: Option<i32>,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

impl Chapter {
    pub fn list(conn: &SqliteConnection) -> Vec<Self> {
        chapter_dsl
            .load::<Chapter>(conn)
            .expect("Error loading users")
    }

    pub fn by_id(id: &str, conn: &SqliteConnection) -> Option<Self> {
        if let Ok(record) = chapter_dsl.find(id).get_result::<Chapter>(conn) {
            Some(record)
        } else {
            None
        }
    }

    pub fn create(
        audiobook_id: &str,
        path: &str,
        title: Option<&str>,
        track: Option<i32>,
        conn: &SqliteConnection,
    ) -> Option<Self> {
        let new_id = Uuid::new_v4().to_hyphenated().to_string();
        // possible data validation here
        // to work on updating
        let new_chapter = Self::new_chapter_struct(&new_id, audiobook_id, path, title, track);
        diesel::replace_into(chapter_dsl)
            .values(&new_chapter)
            .execute(conn)
            .expect("Error saving new user");
        Self::by_id(&new_id, conn)
    }

    pub fn new_chapter_struct(
        id: &str,
        audiobook_id: &str,
        path: &str,
        title: Option<&str>,
        track: Option<i32>,
    ) -> Self {
        Self {
            id: id.into(),
            audiobook_id: audiobook_id.into(),
            path: path.into(),
            title: title.map(Into::into),
            track,
            created_at: chrono::Local::now().naive_local(),
            updated_at: chrono::Local::now().naive_local(),
        }
    }
}
/*
#[cfg(test)]
mod chapter_test;
*/
