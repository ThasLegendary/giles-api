table! {
    audiobook (id) {
        id -> Text,
        path -> Text,
        title -> Nullable<Text>,
        author -> Nullable<Text>,
        reader -> Nullable<Text>,
        genre -> Nullable<Text>,
        year -> Nullable<Integer>,
        cover -> Nullable<Text>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    chapter (id) {
        id -> Text,
        audiobook_id -> Text,
        path -> Text,
        title -> Nullable<Text>,
        track -> Nullable<Integer>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(
    audiobook,
    chapter,
);
